/*******************************************************************************

     Copyright (c) 2006     Bernhard R. Link <brlink@debian.org>

     The X Consortium, and any party obtaining a copy of these files,
     directly or indirectly, is granted, free of charge, a
     full and unrestricted irrevocable, world-wide, paid up, royalty-free,
     nonexclusive right and license to deal in this software and
     documentation files (the "Software"), including without limitation the
     rights to use, copy, modify, merge, publish, distribute, sublicense,
     and/or sell copies of the Software, and to permit persons who receive
     copies from any such party to do so.

     This  program  is  distributed  in  the hope that it will be useful, but
     WITHOUT   ANY   WARRANTY;   without   even  the   implied   warranty  of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

*******************************************************************************/
/* mail header handling stuff */

#include <ctype.h>
#include <string.h>
#include <strings.h>

#include <xbuffy.h>

int cmp_header(const char *buffer, const char *header) {
	size_t len = strlen(header);

	return strncasecmp(buffer, header, len) == 0
		&& buffer[len] == ':';
}

int cmp_header_get(const char *buffer, const char *header, const char **p) {
	size_t len = strlen(header);

	if (strncasecmp(buffer, header, len) == 0
			&& buffer[len] == ':') {
		buffer += len;
		do {
			buffer++;
			while (*buffer == ' ' || *buffer == '\t' ) {
				buffer++;
			}
		} while ( *buffer == '\n' &&
				(buffer[1] == ' ' || buffer[1] == '\t'));
		*p = buffer;
		return 1;
	} else {
		return 0;
	}
}

int proper_mailstart(const char *buffer) {
	char word[4]; int wordlen;

#define FROM "From"
	if (strncmp(buffer,FROM,sizeof(FROM)-1) != 0)
		return 0;
	buffer += sizeof(FROM)-1;
	if (buffer[0] != ' ')
		return 0;
	while (*buffer && isspace(*buffer))
		buffer++;
	/* over from address */
	while (*buffer && !isspace(*buffer)) {
		if (*buffer == '\\') {
			buffer++;
			if (*buffer == '\0' )
				return 0;
			buffer++;
		}
		if (*buffer == '"') {
			while (*buffer != '"') {
				if (*buffer == '\\')
					buffer++;
				if (*buffer == '\0')
					return 0;
				buffer++;
			}
		}
		buffer++;
	}
	if (*buffer == '\0')
		return 0;
	while (*buffer && isspace(*buffer))
		buffer++;
	/* weekday */
	wordlen = 0;
	while (*buffer && !isspace(*buffer)) {
		if (*buffer == '\\') {
			buffer++;
			if (*buffer == '\0' )
				return 0;
			if (wordlen < 3)
				word[wordlen++] = tolower(*buffer);
			buffer++;
		}
		if (*buffer == '"') {
			while (*buffer != '"') {
				if (*buffer == '\\')
					buffer++;
				if (*buffer == '\0')
					return 0;
				if (wordlen < 3)
					word[wordlen++] = tolower(*buffer);
				buffer++;
			}
		} else {
			if (wordlen < 3)
				word[wordlen++] = tolower(*buffer);
		}
		buffer++;
	}
	if (*buffer == '\0')
		return 0;
	while (*buffer && isspace(*buffer))
		buffer++;
	word[wordlen] = '\0';
	switch (word[0]) {
		case 'f':
			if (word[1] != 'r' || word[2] != 'i')
				return 0;
			break;
		case 'm':
			if (word[1] != 'o' || word[2] != 'n')
				return 0;
			break;
		case 's':
			if ((word[1] != 'u' || word[2] != 'n')
			   &&(word[1] != 'a' || word[2] != 't'))
				return 0;
			break;
		case 't':
			if ((word[1] != 'u' || word[2] != 'e')
			   &&(word[1] != 'h' || word[2] != 'u'))
				return 0;
			break;
		case 'w':
			if (word[1] != 'e' || word[2] != 'd')
				return 0;
			break;
		default:
			return 0;
	}
	/* month */
	wordlen = 0;
	while (*buffer && !isspace(*buffer)) {
		if (*buffer == '\\') {
			buffer++;
			if (*buffer == '\0' )
				return 0;
			if (wordlen < 3)
				word[wordlen++] = tolower(*buffer);
			buffer++;
		}
		if (*buffer == '"') {
			while (*buffer != '"') {
				if (*buffer == '\\')
					buffer++;
				if (*buffer == '\0')
					return 0;
				if (wordlen < 3)
					word[wordlen++] = tolower(*buffer);
				buffer++;
			}
		} else {
			if (wordlen < 3)
				word[wordlen++] = tolower(*buffer);
		}
		buffer++;
	}
	if (*buffer == '\0')
		return 0;
	while (*buffer && isspace(*buffer))
		buffer++;
	word[wordlen] = '\0';
	switch (word[0]) {
		case 'a':
			if ((word[1] != 'p' || word[2] != 'r')
			   &&(word[1] != 'u' || word[2] != 'g'))
				return 0;
			break;
		case 'd':
			if (word[1] != 'e' || word[2] != 'c')
				return 0;
			break;
		case 'f':
			if (word[1] != 'e' || word[2] != 'b')
				return 0;
			break;
		case 'j':
			if ((word[1] != 'a' || word[2] != 'n')
			   &&(word[1] != 'u' ||
				   (word[2] != 'n' && word[2] !='l')))
				return 0;
			break;
		case 'm':
			if (word[1] != 'a' ||
				(word[2] != 'r' && word[2] != 'y'))
				return 0;
			break;
		case 'n':
			if (word[1] != 'o' || word[2] != 'v')
				return 0;
			break;
		case 'o':
			if (word[1] != 'c' || word[2] != 't')
				return 0;
			break;
		case 's':
			if (word[1] != 'e' || word[2] != 'p')
				return 0;
			break;
		default:
			return 0;
	}

	/* day of month*/
	wordlen = 0;
	while (*buffer && !isspace(*buffer)) {
		if (*buffer == '\\') {
			buffer++;
			if (*buffer < '0' || *buffer > '9')
				return 0;
			wordlen = wordlen*10+((*buffer)-'0');
			buffer++;
		}
		if (*buffer == '"') {
			while (*buffer != '"') {
				if (*buffer == '\\')
					buffer++;
				if (*buffer < '0' || *buffer > '9')
					return 0;
				wordlen = wordlen*10+((*buffer)-'0');
				buffer++;
			}
		} else {
			if (*buffer < '0' || *buffer > '9')
				return 0;
			wordlen = wordlen*10+((*buffer)-'0');
		}
		buffer++;
	}
	if (wordlen == 0 || wordlen > 31)
		return 0;
	return 1;
}
